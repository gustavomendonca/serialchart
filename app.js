const SerialPort = require('serialport')

// const _ = require('highland')
const Readline = require('@serialport/parser-readline')

const blessed = require('blessed')
const contrib = require('blessed-contrib')

const BAUDRATE = process.env.BAUDRATE || 115200
let serial = null

const getAvailableSerialPorts = () => SerialPort
  .list()
  // .then(results => results.filter(r => r.manufacturer))

screen = blessed.screen()

const page1 = (screen) => {  
  const grid = new contrib.grid({rows: 12, cols: 12, screen: screen})

  let availableSerialPortsList = grid.set(0, 0, 12, 2, blessed.list, { 
    label: 'Available Serial Ports',
    mouse: true,
    style: {
      selected: {
        bg: 'red',
        border: {
          fg: 'blue'
        }
      }
    }
  })

  let chart = grid.set(0, 2, 8, 10, contrib.bar, {
    label: 'Serial Data', 
    barWidth: 6
    , barSpacing: 6
    , xOffset: 0
    , maxHeight: 30
    , minHeight: -30
  })
    
  let serialLog = grid.set(8, 2, 4, 10, contrib.log, {
    label: 'Serial Port Logger',
    fg: "green",
    selectedFg: "green",
    prompt: '>'
  })

  availableSerialPortsList.on('select', item => {
    let comName = item.content
    try {
      if (serial) 
        serial.close(err => serial = new SerialPort(comName, { baudRate: BAUDRATE }))
      else serial = new SerialPort(comName, { baudRate: BAUDRATE })
    }
    catch(err) {
      serialLog.log(`ERROR: ${err}`)
    }

    parser = serial.pipe(new Readline({delimiter: '\n'}))
    
    serial.on('open', () => serialLog.log(`Port ${comName} opened`))
    serial.on('close', () => serialLog.log(`Port ${comName} closed`))
    serial.on('error', err => serialLog.log(`ERROR: ${err}`))
    parser.on('data', data => {
      serialLog.log(data)
      dataParts = data.split(';').map(v => parseFloat(v))
      chartData = { 
        titles: [ 'accX', 'accY', 'accZ', 'gyroX', 'gyroY', 'gyroZ', 'magX', 'magY', 'magZ', 'temp' ], 
        data: dataParts
      }

      // serialLog.log(JSON.stringify(chartData))
      try{
      chart.setData(chartData)
      }catch(err) {serialLog.log('ERROR', err)}
    })
  })

  getAvailableSerialPorts()
    .then(serialports => {
      serialports.forEach(sp => availableSerialPortsList.addItem(sp.comName))
    })
    .catch(err => console.log(err))

}

screen.key(['escape', 'q', 'C-c'], (ch, key) => {
  if (serial && serial.isOpen) serial.close()
  process.exit(0)
})

const carousel = new contrib.carousel( [ page1 ], { 
  screen: screen,
  interval: 0,
  controlKeys: true 
})

carousel.start()
